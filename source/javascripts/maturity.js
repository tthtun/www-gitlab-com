var now = new Date();
var elements = document.getElementsByClassName('today');
Array.prototype.forEach.call(elements, function(element) {
	element.innerHTML = moment(now).format('YYYY-MM-DD');
});