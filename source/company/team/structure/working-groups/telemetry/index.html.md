---
layout: markdown_page
title: "Telemetry Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property     | Value |
|--------------|-------|
| Date Created | May 17, 2019 |
| Date Ended   | TBD |
| Slack        | [#wg_telemetry](https://gitlab.slack.com/messages/CJS56FNFP) (only accessible from within the company) |
| Google Doc   | [Telemetry Working Group Agenda](https://docs.google.com/document/d/1JK_jluomJqZklHL28zSx2Hm5aC0FCGR33ovD1LrN6gM/edit) (only accessible from within the company) |

## Business Goal

Fast track GitLab to becoming a data-driven organisation. For this, there are three main objectives:

* Ensure all usage tracking PII (personally identifiable information) is safely within our own infrastructure.
* Decide on and implement a backend/frontend product usage data tracking solution.
* Decide on an appropriate analysis/visualisation tool and train employees on it.

## Exit Criteria

* The completion and closure of [this epic](https://gitlab.com/groups/gitlab-org/-/epics/1206).

## Roles and Responsibilities

| Working Group Role    | Person                | Title                                        |
|-----------------------|-----------------------|----------------------------------------------|
| Facilitator/Functional Lead            | Luca Williams         | Product Manager, Fulfillment                 |
| Functional Lead       | Chun Du               | Dir of Engineering, Enablement               |
| Functional Lead       | Tim Zallmann          | Dir of Engineering, Dev                      |
| Functional Lead       | Bartek Marnane        | Dir of Engineering, Growth                   | 
| Functional Lead       | Taylor Murphy         | Staff Data Engineer, Architecture            |
| Functional Lead       | Dave Smith            | Manager, Reliability Engineering             |
| Executive Stakeholder | Scott Williamson      | VP of Product                                |
| Executive Stakeholder | Todd Barr             | CMO                                          |
| Member                | Dennis Tang           | FE Engineering Manager, Manage & Fulfillment |
| Member                | James Lopez           | BE Engineering Manager, Fulfillment          |
| Member                | Donald Cook           | FE Engineering Manager, Plan                 |
| Member                | Jeremy Watson         | Sr Product Manager, Manage                   |
| Member                | Eric Brinkman         | Dir of Product, Dev                          |
| Member                | Walter Zabaglio       | Dir of Business Operations                   |
